import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { bigState } from './store/reducers';
import * as TestActions from 'src/app/store/actions' 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'big-angular';

  constructor(private store: Store<bigState>){

  }

  ngOnInit(): void {
    this.store.dispatch(TestActions.getAllPersons());
  }
}
