import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AppRoutingModule } from './app-routing.module';
import { AdminComponent } from './pages/admin.component/admin/admin.component';
import { LoginComponent } from './pages/login.component/login/login.component';
import { PrivacyComponent } from './pages/privacy.component/privacy.component';
import { StoreModule } from '@ngrx/store';
import { testReducer } from './store/reducers';
import { environment } from '../environments/environment';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { EffectsModule } from '@ngrx/effects';
import { TestEffects } from './store/effects';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzCardModule } from 'ng-zorro-antd/card';

registerLocaleData(en);
@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    PrivacyComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({"bigState": testReducer}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    NzTabsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    ReactiveFormsModule,
    NzSelectModule,
    NzGridModule,
    NzTableModule,
    NzDividerModule,
    NzModalModule,
    NzAlertModule,
    NzCardModule,
    NzSpaceModule,
    NzStatisticModule,
    EffectsModule.forRoot([TestEffects]),
  ], 
  providers: [
    { provide: NZ_I18N, useValue: en_US }
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
