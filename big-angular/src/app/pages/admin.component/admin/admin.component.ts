import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {getActivePerson, getActivePersonId, getCheckedInPersons, getCheckedOutPersons } from 'src/app/store/selectors';
import * as TestActions from 'src/app/store/actions' 
import {  Observable } from 'rxjs';
import { Person } from 'src/app/store/interfaces';
import { bigState } from 'src/app/store/reducers';
import { NumberSymbol } from '@angular/common';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html'
})
export class AdminComponent implements OnInit {
  checkedInPersons$:Observable<Person[]>;
  checkedOutPersons$:Observable<Person[]>;
  activePersonId$:Observable<number>;
  activePerson$: Observable<Person>;
  constructor(private store: Store<bigState>) { }

  ngOnInit(): void {
    this.activePersonId$ = this.store.select(getActivePersonId);
    this.checkedInPersons$ = this.store.select(getCheckedInPersons);  
    this.checkedOutPersons$ = this.store.select(getCheckedOutPersons);
    this.activePerson$ = this.store.select(getActivePerson);
  }

  public delete(id: number){
    this.store.dispatch(TestActions.deletePerson({Id: id}));
  }
  public updateActivePerson(id: number){
    this.activePersonId$.subscribe((activeId) => {
      if(activeId != id){
        this.store.dispatch(TestActions.changeActivePersonId({Id: id}));
      }
    }).unsubscribe();  
  }

  public saveEdit(){
    this.store.dispatch(TestActions.editPerson());
  }
  public checkPerson(id: number){
    this.store.dispatch(TestActions.checkPerson({Id: id}));
  }

  public nameChange(name: String){
    this.store.dispatch(TestActions.changeActiveName({Name: name}));
  }
  public emailChange(email: String){
    this.store.dispatch(TestActions.changeActiveEmail({Email: email}));
  }
  public passwordChange(password: String){
    this.store.dispatch(TestActions.changeActivePassword({Password: password}));
  }
}
