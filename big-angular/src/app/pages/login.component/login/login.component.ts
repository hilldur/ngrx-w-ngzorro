import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { addPerson } from 'src/app/store/actions';
import { bigState } from 'src/app/store/reducers';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  validateForm!: UntypedFormGroup;
  gdprAlert: boolean = false;
  captchaTooltipIcon: NzFormTooltipIcon = {
    type: 'info-circle',
    theme: 'twotone'
  };

  constructor(private fb: UntypedFormBuilder,private store: Store<bigState>) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, [Validators.required]],
      checkPassword: [null, [Validators.required, this.confirmationValidator]],
      name: [null, [Validators.required]],
    });
  }

  submitForm(): void {
    this.gdprAlert = false;
    if (this.validateForm.valid) {
      this.store.dispatch(addPerson({Name:this.validateForm.value.name, Email:this.validateForm.value.email, Password:this.validateForm.value.password}));
      this.validateForm.reset();
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
  
  updateConfirmValidator(): void {
    Promise.resolve().then(() => this.validateForm.controls['checkPassword'].updateValueAndValidity());
  }

  confirmationValidator = (control: UntypedFormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls['password'].value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  activateGdprAlert() {
    this.gdprAlert = true;
  }

  cancel(){
    this.gdprAlert = false;
    this.validateForm.reset();
  }
}

