import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { bigState } from 'src/app/store/reducers';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  constructor(private store: Store<bigState>) {}

  ngOnInit(): void {

  }
}
