import { createAction, props } from '@ngrx/store';
import { Person } from './interfaces';


export const addPerson = createAction('[Login Component] Add Person', props<{Name: String, Email: String, Password: String}>());
export const deletePerson = createAction('[Admin Component] Delete Person', props<{Id: number}>());
export const changeActivePersonId = createAction('[Admin Component] Change Active Person Id', props<{Id: number}>());
export const checkPerson = createAction('[Admin Component] Check Person', props<{Id: number}>());
export const editPerson = createAction('[Admin Component] Edit Person');

export const changeActiveName = createAction('[Admin Component] Change ActivePersons Name', props<{Name: String}>());
export const changeActiveEmail = createAction('[Admin Component] Change ActivePersons Email', props<{Email: String}>());
export const changeActivePassword = createAction('[Admin Component] Change ActivePersons Password', props<{Password: String}>());

export const getAllPersons = createAction('[App Component] Get all Persons from DB');
export const getAllPersonsSuccess = createAction('[App Component] Get all Persons from DB Success', props<{Persons: Person[]}>());
export const getAllPersonsFailure = createAction('[App Component] Get all Persons from DB Failure');