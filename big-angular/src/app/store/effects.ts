
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { TestService } from '../testService';
import * as TestActions from '../store/actions'; 
@Injectable()
export class TestEffects {
  constructor(
    private actions$: Actions, private testService: TestService
  ) {}

getAllPersons$ = createEffect(() => this.actions$.pipe(
    ofType(TestActions.getAllPersons),
    mergeMap(() => this.testService.getAllPersons()
      .pipe(
        map(persons => (TestActions.getAllPersonsSuccess({ Persons: persons }))),
        catchError(() => of(TestActions.getAllPersonsFailure()))
      ))
    )
  )
  }