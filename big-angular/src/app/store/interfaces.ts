export interface Person{
    id: number;
    name: String;
    email: String;
    password: String;
    checkedOut: boolean;
}