import { createFeatureSelector, createReducer, on} from '@ngrx/store';
import { Person } from './interfaces';
import { createSelector } from '@ngrx/store';
import * as TestActions from 'src/app/store/actions' 


export interface bigState {
    activePersonId: number,
    persons: Person[],
    activePerson: Person,
}
export const initialState : bigState = {
    activePersonId: -1,
    persons: [],
    activePerson: null,
};

export const testReducer = createReducer(
  
  initialState,
  on(TestActions.addPerson, (state, props) => ({
   ...state,
   persons: [...state.persons,  {id: state.persons.length, name: props.Name, email: props.Email, password: props.Password,checkedOut:false}]
    })),
    on(TestActions.deletePerson, (state, props) => ({
        ...state,
        persons: state.persons.filter(person => person.id !== props.Id)
    })),
    on(TestActions.changeActivePersonId, (state, props) => ({
        ...state,
        activePersonId: state.persons.filter(person => person.id == props.Id)[0].id,
        activePerson: state.persons.filter(person => person.id == props.Id)[0],
    })),
    on(TestActions.checkPerson, (state, props) => ({
        ...state,
        persons: state.persons.map((person) => person.id === props.Id ? {...person, checkedOut: !person.checkedOut} : person)
    })), 
    on(TestActions.editPerson, (state) => ({
        ...state,
        persons: state.persons.map((person) => person.id === state.activePersonId ? state.activePerson : person),
        activePersonId: -1,
        activePerson: null,
    })),
    on(TestActions.getAllPersonsSuccess, (state, props) => ({
        ...state,
        persons: props.Persons
    })),
    on(TestActions.changeActiveName, (state, props) => ({
        ...state,
        activePerson: {...state.activePerson, name: props.Name}
    })), 
    on(TestActions.changeActiveEmail, (state, props) => ({
        ...state,
        activePerson: {...state.activePerson, email: props.Email}
    })), 
    on(TestActions.changeActivePassword, (state, props) => ({
        ...state,
        activePerson: {...state.activePerson, password: props.Password}
    })),
);

