import { createFeatureSelector, createSelector } from "@ngrx/store";
import { bigState } from "./reducers";

export const selectFeatureState = createFeatureSelector<bigState>('bigState');

export const getActivePersonId = createSelector(selectFeatureState, (state: bigState) =>{
    return state.activePersonId
});
export const getAllPersons = createSelector(selectFeatureState, (state: bigState) =>{
    return state.persons
});
export const getCheckedInPersons = createSelector(selectFeatureState, (state: bigState) =>{
    return state.persons.filter((person) => person.checkedOut === false).slice().reverse()
});
export const getCheckedOutPersons = createSelector(selectFeatureState, (state: bigState) =>{
    return state.persons.filter((person) => person.checkedOut === true).slice().reverse()
});
export const getActivePerson = createSelector(selectFeatureState, (state: bigState) =>{
    return state.activePerson
});