import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { observable, Observable, of } from "rxjs";
import { getAllPersons } from "./store/actions";
import { Person } from "./store/interfaces";
@Injectable({ providedIn: 'root' })
export class TestService {
    constructor (private http: HttpClient) {}
  
    getAllPersons() : Observable<Person[]> {
      return of([{id: 0, name: "John Doe", email:"johndoe@gmail.com", password:"google",checkedOut:false},
      {id: 1, name: "Gröt", email:"martina@gmail.com", password:"Brap",checkedOut:false},{id: 2, name: "William", email:"williamB@gmail.com", password:"brajobbatwilliam",checkedOut:false}]);
    }
  }